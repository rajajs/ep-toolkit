EP Toolkit
----------

A web application for providing tools to work with ECGs and Electrophysiology tracings. Primarily to use calipers for making on screen measurements. 

Designed as static page that can work offline and will be installable as a progressive web app.


This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
