let fileInput = document.getElementById('file-input');
let canvas = document.getElementById('caliper-canvas');
let img = new Image();
var info = document.getElementById('info');

// use the service worker
if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("/serviceWorker.js")
      .then(res => console.log("service worker registered"))
      .catch(err => console.log("service worker not registered", err))
  })
}

// Load image from filesystem and display in canvas
fileInput.addEventListener('change', function(ev) {
			       
    if(ev.target.files) {

	info.innerHTML = "Loading ..." ;
	let file = ev.target.files[0];
	var reader  = new FileReader();
	
	reader.onloadend = function (e) {
	    img.src = e.target.result;
	    img.onload = function(ev) {
		canvas.width = img.width;
		canvas.height = img.height;
	    }
	}
	reader.readAsDataURL(file);
	info.innerHTML = "Not calibrated";
	
	// initialise caliper
	var calipercanvas = CaliperCanvas(canvas, img);
    }
})
