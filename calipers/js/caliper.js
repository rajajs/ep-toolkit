
var CaliperCanvas = function (canvas, img) {

    this.canvas = canvas; 
    var cx = canvas.getContext("2d");
    var calibrateButton = document.getElementById('calibrate')

    // var calibration = canvas.getAttribute("img-length") / canvas.getAttribute("width");
    var calibration =  -1; // canvas.getAttribute("calibration")
    
    var buffer = 5; // zone around line to hit
    var touchBuffer = 10;
    var lineColor = "#000000";
    var selLineColor = "#FF0000";
    var mutedLineColor = "#ACACAC";
    var mousepressed = false;
    
    var caliper = {
	x1 : canvas.width * 0.25,
	x2 : canvas.width * 0.5,
	y  : canvas.height * 0.5,
	
	x3 : canvas.width * 0.375,    // midpoint
	x1x2: canvas.width * 0.25,  // measurement
    
	x1Offset : 0,  // offset from mousepos when carrying whole caliper
	x2Offset : 0,
	state : 0,
    };

    // state:
    //        0 - nothing selected
    //        1 - x1 selected
    //        2 - x2 selected
    //        3 - both selected

    // Generic draw line with selected color between coords
    drawLine = function(cx, x1, y1, x2, y2, color) {
	cx.strokeStyle = color;
	cx.beginPath();
	cx.moveTo(x1, y1);
	cx.lineTo(x2, y2);
	cx.stroke();
    }

    
    // Find closest line to mouse position
    findHittable = function(mousex, mousey, caliperx1, caliperx2, calipery, buffer) {
	console.log(mousex, caliperx1, caliperx2, buffer);
	// y - horizontal bar - whole caliper
		if (mousex > caliperx1+buffer &&
		    mousex < caliperx2-buffer &&
		    mousey - calipery > -1 * buffer &&
		    mousey - calipery < buffer
		   ){
		    return 3;
		}

	// x1 - left leg
		else if (mousex - caliperx1 > -1 * buffer &&
			 mousex - caliperx1 < buffer){
		    return 1;
		}

	// x2 - right leg
		else if (mousex - caliperx2 > -1 * buffer &&
			 mousex - caliperx2 < buffer){
		    return 2;
		}

	// nothing hittable
		else {
		    return 0;
		}	
    }

    // Find closest line to touch position
    // wider buffer for touch. anything from left edge to x1 + buffer for left, x2-buffer to right edge for right and all else for whole
    findHittableTouch = function(touchx, touchy, caliperx1, caliperx2, calipery, touchBuffer) {

	if (touchx < caliperx1 + touchBuffer) {
	    return 1;
	}

	else if (touchx > caliperx2 - buffer) {
	    return 2;
	}

	else {
	    return 3;
	}
    }

    
    // caliper measurement
    updateMeasurement = function (caliper) {
	caliper.x3 = (caliper.x1 + caliper.x2) / 2;
	caliper.x1x2 = Math.floor(Math.abs(caliper.x2 - caliper.x1));	
    }
    
    
    // caliper draw
    caliper.draw = function (cx, height) {
	// draw x1 line
	if (caliper.state == 1 || caliper.state == 3) {
	    drawLine(cx, caliper.x1, 0, caliper.x1, height, selLineColor);
	}
	else {
	    drawLine(cx, caliper.x1, 0, caliper.x1, height, lineColor);	    
	}

	// draw horizontal line
	if (caliper.state == 3) {
	    drawLine(cx, caliper.x1, caliper.y, caliper.x2, caliper.y, selLineColor);
	}
	else {
	    drawLine(cx, caliper.x1, caliper.y, caliper.x2, caliper.y, lineColor);	    
	}
    
	// draw x2 line
	if (caliper.state == 2 || caliper.state == 3) {
	    drawLine(cx, caliper.x2, 0, caliper.x2, height, selLineColor);
	}
	else {
	    drawLine(cx, caliper.x2, 0, caliper.x2, height, lineColor);	    
	}

	//
	if (calibration != -1) {
	    writeMeasurement(cx, Math.floor(caliper.x1x2 * calibration),
			     caliper.x3, caliper.y - 40, 'ms');
	} else {
	    writeMeasurement(cx, Math.floor(caliper.x1x2),
			     caliper.x3, caliper.y - 40, 'px');
	}
	
	// vertical mid
	drawLine(cx,
		 caliper.x3, caliper.y + height / 20,
		 caliper.x3, caliper.y - height / 20,
		 mutedLineColor)

	// right vertical
	drawLine(cx,
		 caliper.x3 + caliper.x1x2 * 1.5, caliper.y + height / 20,
		 caliper.x3 + caliper.x1x2 * 1.5, caliper.y - height / 20,
		 mutedLineColor)

	// left vertical
	drawLine(cx,
		 caliper.x3 - caliper.x1x2 * 1.5, caliper.y + height / 20,
		 caliper.x3 - caliper.x1x2 * 1.5, caliper.y - height / 20,
		 mutedLineColor)	
	
	writeMuted(cx, '+1', caliper.x3 + 5 + caliper.x1x2 * 1.5, caliper.y);
	writeMuted(cx, '-1', caliper.x3 - 10 - caliper.x1x2 * 1.5, caliper.y);
	writeMuted(cx, '0.5', caliper.x3 + 5, caliper.y);
    }


    // Load image
    // var img = document.createElement("img");
    // img.src = canvas.getAttribute("img-src");

    // Get mouse position
    function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
        var elRelX = evt.clientX - rect.left	
	var elRelY = evt.clientY - rect.top
	var canvRelX = elRelX * canvas.width / rect.width;
	var canvRelY = elRelY * canvas.height / rect.height;
	
	return {
	    x: canvRelX,
	    y: canvRelY            
	};
    }

    // Get position of single touch
    function getTouchPos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	if (evt.targetTouches.length == 1) {
	    var touch = evt.targetTouches[0];
	    return {
		x: touch.clientX - rect.left,
		y: touch.clientY - rect.top
	    };
	}}
    

    calibrate.addEventListener('click', function (evt) {
	var measurement=prompt("Current measurement in ms");
	if (measurement != null) {
	    calibration = measurement / caliper.x1x2;
	}
    })
    
    function writeMeasurement(context, message, x, y, units) {
	context.font = '12pt Calibri';
	context.fillStyle = 'black';
	context.fillText(message + ' ' + units, x, y);
    }

    function writeMuted(context, message, x, y) {
	context.font = '8pt Calibri';
	context.fillStyle = 'grey';
	context.fillText(message, x, y);
    }


    function frameDraw () {
	// draw routine
	cx.clearRect(0, 0, canvas.width, canvas.height);
	cx.drawImage(img, 0, 0, img.width, img.height,
		     0, 0, canvas.width, canvas.height);
	caliper.draw(cx, canvas.height);	
    }


    // touch events
    canvas.addEventListener('touchstart', function (evt) {
	if (evt.targetTouches.length > 1) {
	    
	} else {
	    var touchPos = getTouchPos(canvas, evt);
	    caliper.state = findHittableTouch(touchPos.x, touchPos.y,
					      caliper.x1, caliper.x2, caliper.y,
					      touchBuffer);
	    
	    // Need an offset always because we use a large buffer for touch
	    if (caliper.state != 0) {
		caliper.x1Offset =  caliper.x1 - touchPos.x; 
		caliper.x2Offset = caliper.x2 - touchPos.x;
	    }
	    
	    frameDraw();
	}
    })

    canvas.addEventListener('touchend', function (evt) {
	caliper.state = 0;
	// swap x1 x2 if required
	if (caliper.x2 < caliper.x1){
	    caliper.x2 = [caliper.x1, caliper.x1 = caliper.x2][0];
	}	
	frameDraw();
    })
    
    
    // determine mousepressed state
    canvas.addEventListener('mousedown', function(evt) {
	console.log('mouse down')
	var mousePos = getMousePos(canvas, evt);
	// find caliper leg to grab
	caliper.state = findHittable(mousePos.x, mousePos.y,
				     caliper.x1, caliper.x2, caliper.y,
				     buffer);
	console.log(caliper.state);
	// calculate offsets if whole caliper selected
	if (caliper.state == 3) {
	    caliper.x1Offset = caliper.x1 - mousePos.x;
	    caliper.x2Offset = caliper.x2 - mousePos.x;
	}

	frameDraw();
	
	mousepressed = true;
    })

    canvas.addEventListener('mouseup', function(evt) {
	caliper.state = 0  	// mouse up releases any held caliper

	// swap x1 x2 if required
	if (caliper.x2 < caliper.x1){
	    caliper.x2 = [caliper.x1, caliper.x1 = caliper.x2][0];
	}

	frameDraw();
	
	mousepressed = false;
    })


    // move
    canvas.addEventListener('mousemove', function(evt) {
	var mousePos = getMousePos(canvas, evt);
	
	if (mousepressed) {

	    switch (caliper.state) {

		// nothing selected
	    case 0:
		break;
		
		// move left leg
	    case 1:
		caliper.x1 = mousePos.x;
		caliper.y = mousePos.y;
		updateMeasurement(caliper);
		break;
		
		// move right leg
	    case 2:
		caliper.x2 = mousePos.x;
		caliper.y = mousePos.y;
		updateMeasurement(caliper);
		break;
		
		// move whole caliper
	    case 3:
		caliper.x1 = mousePos.x + caliper.x1Offset;
		caliper.x2 = mousePos.x + caliper.x2Offset;
		caliper.y = mousePos.y;
		updateMeasurement(caliper);
		break;
	    }
		
	    frameDraw();
	    
	}
	else {
	}
    })

    
    // touch move
    canvas.addEventListener('touchmove', function (evt) {
	// handle only single touch events
	if (evt.targetTouches.length > 1) {
	    // console.log('more than one touch');
	    // zoom works !
	} else {

	    var touchPos = getTouchPos(canvas, evt);
	    
	    switch (caliper.state) {
		
	    case 0:
		break;
		
	    case 1:
		caliper.x1 = touchPos.x + caliper.x1Offset;
		caliper.y = touchPos.y;
		updateMeasurement(caliper);
		break;
		
	    case 2:
		caliper.x2 = touchPos.x + caliper.x2Offset;
		caliper.y = touchPos.y;
		updateMeasurement(caliper);
		break;
		
	    case 3:
		caliper.x1 = touchPos.x + caliper.x1Offset;
		caliper.x2 = touchPos.x + caliper.x2Offset;	    
		caliper.y = touchPos.y;
		updateMeasurement(caliper);
		break;
	    }
	    
	    frameDraw();

	    // touchmove wont scroll when on canvas now
	    // evt.preventDefault();
	    evt.stopPropagation();
	}
    })
    

    // Draw everything in beginning
    frameDraw();
    
}


